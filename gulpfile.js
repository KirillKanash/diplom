const gulp= require('gulp');
const sass = require('gulp-sass');
const watch = require ('gulp-watch');
const gulpFont = require ('gulp-font');
const image = require('gulp-image');
const browserSync = require('browser-sync');


gulp.task('sass', function () {
    return gulp.src('./source/styles/main.scss')          //адреc файла
      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest('./build/styles'));
  });


 
gulp.task('image', function () {
  gulp.src('./source/images/*')
    .pipe(image())
    .pipe(gulp.dest('./build/images'));
});



gulp.task('fonts', function () {
    gulp.src('./source/fonts/*')
      .pipe(image())
      .pipe(gulp.dest('./build/fonts'));
  });

gulp.task('html', function () {
gulp.src('./source/pages/*')
    .pipe(image())
    .pipe(gulp.dest('./build/'));
});


gulp.task ('browserSync', function(){
    browserSync({
        server:{baseDir:'./build/'},
    });
});


gulp.task('watch',function(){
    gulp.watch('./source/styles/main.scss',['sass']);
    gulp.watch('./source/images/*',['image']);
    gulp.watch('./source/fonts/*',['fonts']);
    gulp.watch('./source/pages/index.html',['html']);
    gulp.watch('./source/styles/main.scss').on('change', browserSync.reload)
    gulp.watch('./source/pages/index.html').on('change', browserSync.reload)
    gulp.watch('./source/images/*').on('change', browserSync.reload)
    gulp.watch('./source/fonts/*').on('change', browserSync.reload)
});





gulp.task('default',['sass','image','fonts','html', 'browserSync', 'watch'],function(){});




