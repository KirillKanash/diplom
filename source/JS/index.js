//Firstscreen
var head=document.querySelector(".slider__novbar");
window.addEventListener("scroll", changecolor)
 function changecolor(){
     if (window.pageYOffset>750){
        head.style.backgroundColor="#333b46";
        
     }
     else {
        head.style.backgroundColor="rgba(0, 120, 201, 0.0)";
       
     }
 }


//slider
var allSlides = document.querySelectorAll('.details__container');
allSlides[0].style.width = '1030px';
allSlides[0].style.opacity = '1';
allSlides[0].style.margin = "0 auto";
var allNavs = document.querySelectorAll('.details__slider');
console.log(allSlides)
console.log(allNavs)
for(var i = 0; i< allNavs.length; i++) {
    allNavs[i].addEventListener('click', function(event){
        for(var x=0; x<allNavs.length; x++) {
            if(allNavs[x] == event.currentTarget) {
                allSlides[x].style.width = "1030px";
                allSlides[x].style.opacity = "1";
                allSlides[x].style.margin = "0 auto";
            } else {
                allSlides[x].style.width = "0px";
                allSlides[x].style.opacity = "0";
                allSlides[x].style.margin = "0px";
                
            }
        }
    });
}



//MAP
var map=document.querySelector(".map__title");
map.addEventListener("click",Map);
function Map(){
    map.style.display="none";
}


//video
var video=document.querySelector(".blog__playvideo");
var img=document.querySelector('.blog__img')
img.addEventListener('click',function video(){
    img.style.display="none";
    video.style.display="block";
})
console.log(video)
console.log(img)


//BLOG
var blogSlides = document.querySelectorAll('.blog__item');
blogSlides[0].style.display = 'flex';
var blogNavs = document.querySelectorAll('.blog__circle');
console.log(blogSlides)
console.log(blogNavs)
for(var i = 0; i< blogNavs.length; i++) {
    blogNavs[i].addEventListener('click', function(event){
        for(var x=0; x<blogNavs.length; x++) {
            if(blogNavs[x] == event.currentTarget) {
                blogSlides[x].style.display = "flex";
                
            } else {
                blogSlides[x].style.display = "none";
               
                
            }
        }
    });
}

//TESTIMONIALS
var testimonialsSlides = document.querySelectorAll('.testimonials__content');
testimonialsSlides[0].style.display = 'flex';
var testimonialsNavs = document.querySelectorAll('.testimonials__circle');
console.log(testimonialsSlides)
console.log(testimonialsNavs)
for(var i = 0; i< testimonialsNavs.length; i++) {
    testimonialsNavs[i].addEventListener('click', function(event){
        for(var x=0; x<testimonialsNavs.length; x++) {
            if(testimonialsNavs[x] == event.currentTarget) {
                testimonialsSlides[x].style.display = "flex";
            } else {
                testimonialsSlides[x].style.display = "none";    
            }
        }
    });
}


//TEAM
var teamSlides = document.querySelectorAll('.team__content');
teamSlides[0].style.display = 'flex';
var teamNavs = document.querySelectorAll('.team__circle');
console.log(teamSlides)
console.log(teamNavs)
for(var i = 0; i< teamNavs.length; i++) {
    teamNavs[i].addEventListener('click', function(event){
        for(var x=0; x<teamNavs.length; x++) {
            if(teamNavs[x] == event.currentTarget) {
                teamSlides[x].style.display = "flex";
            } else {
                teamSlides[x].style.display = "none";    
            }
        }
    });
}


//BURGER
var burger=document.querySelector(".slider__burger")
var close=document.querySelector(".slider__close")
var nav=document.querySelector(".slider__burger--click")

burger.addEventListener("click",function open(){
    burger.style.display="none";
    close.style.display="flex";
    nav.style.display="flex";
})
close.addEventListener("click", function clo(){
    burger.style.display="flex";
    close.style.display="none";
    nav.style.display="none";
})

window.addEventListener("scroll", changeback)
 function changeback(){
     if (window.pageYOffset>750){
        nav.style.backgroundColor="#333b46";
        
     }
     else {
        nav.style.backgroundColor="rgba(0, 120, 201, 0.0)";
       
     }
 }
